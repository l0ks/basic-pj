package com.company;

public class HelloWordGen {
    private static String hello = "Hello";
    private static String world = "World";

    public String helloWorld(){
        return hello + " " + world;
    }
}
